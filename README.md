# Sitemap Generator

Takes a list of paths to make a base sitemap, and generate sitemaps for our podcast pages.  
Then make a sitemap index to list them all.

## Install

- Copy `dbconfig.example.php` to `dbconfig.php`
- Change `dbconfig.php` values like it how it's done in [rb-vue](https://gitlab.com/radiobrony/rb-vue)
- Change the Base URL and paths at the top of `cron.php` if needed
- Install dependecies: `composer install`
- Run it once: `php cron.php`
- Add a cron: `0 * * * * /usr/bin/php /var/www/rb-sitemap/cron.php`
- Make nginx load our folder:
  ```nginx
  location ^~ /sitemaps/ {
    alias /var/www/rb-sitemap/sitemaps/;
    index index.xml;
  }
  ```

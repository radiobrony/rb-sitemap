<?php
// --- CONFIG
$baseURL = 'https://radiobrony.fr';
$basePages = [
  '/',
  '/live',
  '/faq',
  '/privacy',
  '/equipe',
  '/podcasts',
  '/friendship-is-magic-ce-que-pourraient-apprendre-de-mlp-les-artistes-independants',
  '/dossier-comment-et-ou-trouver-de-la-musique-brony-aujourdhui'
];
require __DIR__ . '/dbconfig.php';


// --- SETUP

// Require composer autoloader
require __DIR__ . '/vendor/autoload.php';

// Create DB instance
use Medoo\Medoo;

// Initialize DB
$db = new Medoo($dbconfig);

// Storing our sitemaps filenames here
$sitemapsIndex = [];


// -- FUNCTIONS

function baseMapGenerator () {
  // Make a single .xml with our base pages
  global $sitemapsIndex, $baseURL, $basePages;

  $xmlBase = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

EOF;

  foreach ($basePages as $page) {
    if ($page == '/') $changeFreq = 'weekly';
    elseif ($page == '/live') $changeFreq = 'daily';
    else $changeFreq = 'monthly';

    $xmlBase .= <<<EOF
  <url>
    <loc>${baseURL}${page}</loc>
    <changefreq>${changeFreq}</changefreq>
    <priority>1.0</priority>
  </url>

EOF;
  }

  $xmlBase .= "</urlset>";

  file_put_contents(__DIR__ . '/sitemaps/sitemap_base.xml', $xmlBase);
  $sitemapsIndex[] = 'sitemap_base.xml';
}

function podcastMapGenerator ($slug) {
  // Make a .xml.gz file for each podcast
  // Has the show page and the last 999 episodes
  global $db, $sitemapsIndex, $baseURL;

  $xmlPod = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  <url>
    <loc>${baseURL}/podcasts/show/${slug}</loc>
    <changefreq>weekly</changefreq>
    <priority>0.5</priority>
  </url>

EOF;

  $episodes = $db->select('pod_episodes', [
    'season',
    'episode',
    'pubDate'
  ], [
    'showSlug' => $slug,
    'LIMIT' => 999,
    'ORDER' => [
      'pubDate' => 'DESC'
    ]
  ]);

  foreach ($episodes as $ep) {
    $lastMod = date("Y-m-d", strtotime($ep['pubDate']));

    $xmlPod .= <<<EOF
  <url>
    <loc>${baseURL}/podcasts/show/${slug}/episode/${ep['season']}/${ep['episode']}</loc>
    <lastmod>${lastMod}</lastmod>
    <priority>0.8</priority>
  </url>

EOF;
  }

  $xmlPod .= "</urlset>";

  /* Possible GZIP
  $gz = gzopen(__DIR__ . '/sitemaps/sitemap_podcast_' . $slug . '.xml.gz','w9');
  gzwrite($gz, $xmlPod);
  gzclose($gz);
  */

  file_put_contents(__DIR__ . '/sitemaps/sitemap_podcast_' . $slug . '.xml', $xmlPod);
  $sitemapsIndex[] = 'sitemap_podcast_' . $slug . '.xml';
}

function indexGenerator () {
  // Take all the sitemaps and make an index file
  global $sitemapsIndex, $baseURL;

  $xmlIndex = <<<EOF
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

EOF;

  foreach ($sitemapsIndex as $file) {
    $xmlIndex .= <<<EOF
  <sitemap>
    <loc>${baseURL}/sitemaps/${file}</loc>
  </sitemap>

EOF;
  }

  $xmlIndex .= "</sitemapindex>";

  file_put_contents(__DIR__ . '/sitemaps/index.xml', $xmlIndex);
}


// --- RUN

// First sitemap
baseMapGenerator();

// Looping thru our shows
$shows = $db->select('pod_shows', [ 'slug' ]);
foreach ($shows as $show) {
  podcastMapGenerator($show['slug']);
}

// Finally our index of sitemaps
indexGenerator();
